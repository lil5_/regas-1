import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';

import 'vuetify/dist/vuetify.min.css';

import store from './store/index';
import App from './App';

// theme
import colors from 'vuetify/es5/util/colors';

Vue.use(Vuex);
Vue.use(Vuetify, {
	theme: {
		primary: colors.brown.darken1,
		secondary: colors.brown.lighten3,
		accent: colors.amber.lighten4
	},
});

new Vue({
	store: new Vuex.Store(store),
	render: h => h(App)
}).$mount('#app');
