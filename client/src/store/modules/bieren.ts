import axios from 'axios';

const api = axios.create({
	baseURL: 'http://localhost:3000/',
})

interface stateInterface {
	all: array,
	p: int,
	sizep: int,
}
const state: stateInterface = {
	all: [],
	p: 0, // page
	sizep: 64, // products per page
};

const getters = {};
const actions = {
	getNextBier({ dispatch, state }) {
		console.log('nextPage', state.p)
		dispatch('getBier', state.p + 1);
	},
	async getBier({ commit, state, rootState }, p: int) {
		// get search queries
		let bierQ: string = rootState.filter.bier.length > 0
			? `&Bier_like=${rootState.filter.bier}`
			: ''
		let brouwerijQ: string = rootState.filter.brouwerij.length > 0
			? `&Brouwerij_like=${rootState.filter.brouwerij}`
			: ''
		let sortQ: string = `&_sort=Brouwerij&_order=asc`

		let paginate = `?_start=${p * state.sizep}&_end=${(1 + p) * state.sizep}`
		let response = await api.get(`/bieren${paginate + bierQ + brouwerijQ + sortQ}`)

		let totalPages: int = Math.ceil(response.headers['x-total-count'] / state.sizep);
		// totalPages starts at 1 while p starts at 0
		let isLastPage: boolean = (totalPages - 1) === p;
		console.log(`tot: ${totalPages}, last: ${isLastPage}, p: ${p}`)
		console.log(paginate)

		console.log(response)
		console.log('p', p)
		commit('fill', { data: response.data, p });
		if(isLastPage) commit('isLastPage');
	}
};
const mutations = {
	isLastPage() {
		state.p = -1
	}
	fill(state, { data, p }) {
		console.log('fill', p)
		// if getting page 0 flush old data
		if(p === 0) {
			state.all = data
			state.p = 0
		}
		// else push new to old data
		else {
			state.p += 1
			state.all = [
				...state.all,
				...data,
			];
		}
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
