import axios from 'axios';

interface stateInterface {
	bier: string,
	brouwerij: string,
}
const state: stateInterface = {
	// search terms
	bier: '',
	brouwerij: '',
};

const getters = {};
const actions = {
	search({ commit, dispatch }, filterObj: object) {
		// change filter state
		for (let key:string in filterObj) {
			switch(key) {
				case 'bier':
					commit('updateBier', filterObj[key]);
					break;
				case 'brouwerij':
					commit('updateBrouwerij', filterObj[key]);
					break;
			}
		}

		// call api with new  filter
		dispatch('bieren/getBier', 0, { root: true })
	}
};
const mutations = {
	updateBier (state, bier: string) {
		state.bier = bier
	}
	updateBrouwerij (state, brouwerij: string) {
		state.brouwerij = brouwerij
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
