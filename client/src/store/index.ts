import bieren from './modules/bieren';
import filter from './modules/filter';

const debug = process.env.NODE_ENV !== 'production';

export default {
	modules: {
		bieren,
		filter
	},
	strict: debug
};
