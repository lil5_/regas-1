Als backend voor dit project, wordt json-server gebruikt. 

json-server is een tool die een backend restfull API maakt op basis van een json bestand.
json-server werkt met node.

Er zitten twee bestand bij in begrepen. db.json is de database, db-original is een backup.



installatie json-server:

1. npm i -g json-server

gebruik json-server:

1. ga in je terminal/opdrachtprompt naar de locatie waar db.json staat
2. voer de volgende opdracht uit: json-server --watch db.json

als je nu in je browser naar localhost:3000/bieren/1 gaat zie je het volgende

{
  "id": 1,
  "Bier": "1410",
  "Stijl": "speciaalbier",
  "Stamwortgehalte": "13.0",
  "Alcoholpercentage": "5,5%",
  "Gisting": "hoog",
  "Sinds": 2010,
  "Brouwerij": "SNAB"
}

voor meer informatie over json-server zie: https://github.com/typicode/json-server