# Regas Assessment

- Trello project management: https://trello.com/b/QG7utIzd/regas-1
- Assessment: [email](/assessment.txt)

## Setup

```
cd server
yarn && yarn run start
```

```
cd client
yarn && yarn run start
```

## Development

please go to [`develop`](https://bitbucket.org/lil5_/regas-1/branch/develop) branch.
